<?php

namespace Doctoreto\Messenger\Channels;

use GuzzleHttp\Client;
use Illuminate\Notifications\Notification;

class SmsChannel
{
    /**
     * The Guzzle client instance.
     *
     * @var \GuzzleHttp\Client
     */
    protected $client;

    /**
     * The phone number notifications should be sent from.
     *
     * @var string
     */
    protected $from;

    /**
     * Create a new Sms channel instance.
     *
     * @return void
     */
    public function __construct()
    {
        $kavenegarConfig = config('services.kavenegar');

        if (is_null($kavenegarConfig)) {
            throw new \InvalidArgumentException('In order to send sms via kavenegar you need to add credentials in the `kavenegar` key of `config.services`.');
        }

        $this->client = new Client([
            'base_uri' => "https://api.kavenegar.com/v1/{$kavenegarConfig['api_key']}/sms/",
        ]);
    }

    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return \GuzzleHttp\Psr7\Response
     */
    public function send($notifiable, Notification $notification)
    {
        if (!$to = $notifiable->routeNotificationFor('sms')) {
            return;
        }

        if (app()->environment() !== 'production') {
            $to = "09178882560";
        }

        $message = $notification->toSms($notifiable);

        $header = $message->header ? $message->header . " " : '';
        $footer = $message->footer ? " " . $message->footer : '';
        $content = $header . $message->content . $footer;

        return $this->client->get(
            'send.json?' .
            "receptor=" . $to .
            "&sender=" . $message->from .
            "&message=" . $content
        );
    }
}
