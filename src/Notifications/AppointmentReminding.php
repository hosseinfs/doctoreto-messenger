<?php

namespace Doctoreto\Messenger\Notifications;

use Carbon\Carbon;
use Doctoreto\Messenger\Channels\PusheChannel;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class AppointmentReminding extends Notification implements ShouldQueue
{
    use Queueable;

    protected $doctorName;
    protected $datetime;

    public $all = false;

    public $tries = 0;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        string $doctorName,
        string $datetime
    ) {
        $this->doctorName = $doctorName;
        $this->datetime = $datetime;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [PusheChannel::class];
    }

    public function toPushe($notifiable)
    {
        $twoHourDate = Carbon::createFromFormat('Y-m-d H:i:s',$this->datetime,'iran');
        $twoHourDate->subHours(2);
        $oneDayDate = Carbon::createFromFormat('Y-m-d H:i:s',$this->datetime,'iran');
        $oneDayDate->subDay(1);

        return [
            "id1" => 1004,
            "id2" => 1204,
//            "id3" => 10035,
            "rate" => 0,
            "bookedId" => 100,
            "image" => "",
//            "name" => "دکتر علی اصغری",
//            "speciality" => "متخصص زنان",
//            "title3" => "لطفا به ویزیت قبلی خود امتیاز دهید",
//            "message3" => "پیام سوم",
//            "date3" => "2018-10-12 14:00:00",
            "seconds1" => '$dateGapInDays==0 ? 0 => ((24*($dateGapInDays-1))*60*60)',
            "seconds2" => '$dateGapInSeconds>8000 ? $dateGapInSeconds-7200 => 0',
            "date1" => $oneDayDate->format('Y-m-d H:i:s'),
            "date2" => $twoHourDate->format('Y-m-d H:i:s'),
            "title1" => "یادآوری نوبت",
            "title2" => "یادآوری نوبت",
            "message1" => "شما فردا ساعت " . render_persian_number($oneDayDate->format('H:i')) . " یک نوبت پیش رو از دکتر " . $this->doctorName . " دارید.",
            "message2" => "شما دو ساعت دیگر " . render_persian_number($oneDayDate->format('H:i')) . " یک نوبت پیش رو از دکتر " . $this->doctorName . " دارید.",
            "type" => "setAlarm"
        ];

    }
}
