<?php

namespace Doctoreto\Messenger\Notifications;

use Doctoreto\Messenger\Values\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Doctoreto\Messenger\Channels\SmsChannel;
use Doctoreto\Messenger\Messages\SmsMessage;

class UserAppointmentCancelledByAdmin extends Notification implements ShouldQueue
{
    use Queueable;

    protected $doctorName;


    protected $returnCredit;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        string $doctorName,
        bool $returnCredit
    )
    {
        $this->doctorName = $doctorName;
        $this->returnCredit = $returnCredit;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        $credit = $this->returnCredit ? ' مبلغ پرداختی شما به حساب کاربریتان افزوده شد و میتوانید مجددا نوبت گیری نمایید.' : '';
        return (new SmsMessage())
            ->content(
                "دکترِتو: نوبت شما از دکتر " . $this->doctorName .
                " از جانب دکترِتو لغو گردید."
                . $credit
                . "%0a" . Constants::SUPPORT_NUMBER
            );
    }
}
