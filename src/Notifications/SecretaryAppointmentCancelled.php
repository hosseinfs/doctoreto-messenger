<?php

namespace Doctoreto\Messenger\Notifications;

use Doctoreto\Messenger\Values\Constants;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Doctoreto\Messenger\Channels\SmsChannel;
use Doctoreto\Messenger\Messages\SmsMessage;

class SecretaryAppointmentCancelled extends Notification implements ShouldQueue
{
    use Queueable;

    protected $patientName;
    protected $by;
    protected $doctorName;
    protected $date;
    protected $hour;
    
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(
        string $patientName,
        string $doctorName,
        string $date,
        string $hour,
        string $by
    ) {
        $this->by = $by;
        $this->patientName = $patientName;
        $this->doctorName = $doctorName;
        $this->date = $date;
        $this->hour = $hour;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return (new SmsMessage())
            ->header("دکترتو:")
            ->content(
                "نوبت " . $this->patientName .
                " از دکتر " . $this->doctorName .
                " در روز " . $this->date .
                " ساعت " . $this->hour .
                " توسط " . $this->by .
                " لغو گردید."
                . "%0a" . Constants::SUPPORT_NUMBER
            );
    }
}
